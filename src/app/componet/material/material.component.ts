import { Component, OnInit } from '@angular/core';

import { GuardarService } from '../../service/guardar.service';

@Component({
  selector: 'app-material',
  templateUrl: './material.component.html',
  styleUrls: ['./material.component.css']
})
export class MaterialComponent implements OnInit {

  constructor(private _guardarService: GuardarService) { 
    
  }

  ngOnInit() {
  }

  guardar(key, cantidad){
    this._guardarService.guardarString(key, cantidad);
  }

  getDatos(key) {
    return this._guardarService.getData(key);
  }
}
