import { Component, OnInit } from '@angular/core';

import { GuardarService } from '../../service/guardar.service';

@Component({
  selector: 'app-instaladores',
  templateUrl: './instaladores.component.html',
  styleUrls: ['./instaladores.component.css']
})
export class InstaladoresComponent implements OnInit {
  private instaladores: any[] = [];
  constructor(private _guardarService: GuardarService) {
    this.recuperar();
  }

  ngOnInit() {
    this.instaladores = this._guardarService.getData("instalador")
  }

  guardar(obj){
    if(this.instaladores != null) {
      this.instaladores.push(obj)
      this._guardarService.guardarObjeto("instalador", this.instaladores)
    } else {
      let temp = [];
      temp.push(obj);
      this._guardarService.guardarObjeto("instalador", temp)      
    }
  }
  recuperar() {
    //console.log(this._guardarService.getData("instalador"));
    this.instaladores = this._guardarService.getData("instalador");
  }

  actualizar(datos) {
    console.log(datos.name);
    let material = this._guardarService.getData(datos.name);
    let total = material + datos.value;
    console.log(total);
    this._guardarService.guardarString(datos.name, total);
  }
}
