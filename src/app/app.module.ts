import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';
// ROUTE
import { AppRoutingModule } from './app-routing.module';
// COMPONENTS
import { AppComponent } from './app.component';
import { InstaladoresComponent } from './componet/instaladores/instaladores.component';
import { MaterialComponent } from './componet/material/material.component';
// SERVICE
import { GuardarService } from './service/guardar.service';

@NgModule({
  declarations: [
    AppComponent,
    InstaladoresComponent,
    MaterialComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [
    GuardarService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
