import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GuardarService {

  constructor() { }

  guardarObjeto(key, obj) {
    localStorage.setItem(key, JSON.stringify(obj));
  }

  guardarString(key, string) {
    localStorage.setItem(key, string);
  }

  getData(key) {
    return JSON.parse(localStorage.getItem(key));
  }
}
